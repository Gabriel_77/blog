﻿using blog_front.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;


namespace blog_front.Controllers
{
    public class PostController : Controller
    {
        HttpClient httpClient = new HttpClient();
        

        public PostController()
        {
            httpClient.BaseAddress = new Uri("https://localhost:44309/");
        }
        // GET: PostController
        public ActionResult Index()
        {
            
            var request= httpClient.GetAsync("api/Posts").Result;
            if (request.IsSuccessStatusCode)
            {
                var result = request.Content.ReadAsStringAsync().Result;
                var listado = JsonConvert.DeserializeObject<List<Post>>(result);
                return View(listado);
            }

            return View();
        }

        // GET: PostController/Details/5
        public ActionResult Details(int id)
        {
            
            var request = httpClient.GetAsync("api/Posts/" + id).Result;
            if (request.IsSuccessStatusCode)
            {
                var result = request.Content.ReadAsStringAsync().Result;
                var post = JsonConvert.DeserializeObject<Post>(result);
                return View(post);
            }
            return View();
        }

        // GET: PostController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PostController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post)
        {
            try
            {
                var request = httpClient.PostAsync("api/Posts/", post, new JsonMediaTypeFormatter()).Result;
                if (request.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }
                
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PostController/Edit/5
        public ActionResult Edit(int id)
        {            
            var request = httpClient.GetAsync("api/Posts/" + id).Result;
            if (request.IsSuccessStatusCode)
            {
                var result = request.Content.ReadAsStringAsync().Result;
                var post = JsonConvert.DeserializeObject<Post>(result);
                return View(post);
            }            

            return View();
        }

        // POST: PostController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Post post)
        {
            try
            {
                post.Id = id;                
                var data = JsonConvert.SerializeObject(post);
                HttpContent httpContent = new StringContent(data, Encoding.UTF8, "application/json");

                var request = httpClient.PatchAsync("api/Posts/" + id, httpContent).Result;
                if (request.IsSuccessStatusCode)
                {                    
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View(id);
                }
                
            }
            catch
            {
                return View();
            }
        }



        // GET: PostController/Delete/5
        public ActionResult Delete(int id)
        {
            var request = httpClient.GetAsync("api/Posts/" + id).Result;
            if (request.IsSuccessStatusCode)
            {
                var result = request.Content.ReadAsStringAsync().Result;
                var post = JsonConvert.DeserializeObject<Post>(result);
                return View(post);
            }
            return View();
        }

        // POST: PostController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, Post post)
        {
            try
            {
                var request = httpClient.DeleteAsync("api/Posts/" + id).Result;
                if (request.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View(id);
                }

            }
            catch
            {
                return View(id);
            }
        }
    }
}
