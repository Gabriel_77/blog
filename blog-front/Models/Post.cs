﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace blog_front.Models
{
    public class Post
    {
        public int Id { get; set; }
        [Required]
        [MinLength(2, ErrorMessage = "Debe ingresar al menos 2 caracteres")]
        public string Titulo { get; set; }
        [Required]
        [MinLength(10, ErrorMessage = "Debe ingresar no menos 10 caracteres")]
        public string Contenido { get; set; }
        public string Imagen { get; set; }
        public string Categoria { get; set; }
        [DataType(DataType.Date)]
        public DateTime? FechaCreacion { get; set; }
        public byte Estado { get; set; }

    }
}
