USE [blogDB]
GO
/****** Object:  Table [dbo].[blog]    Script Date: 23/4/2021 16:36:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[blog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Titulo] [varchar](50) NOT NULL,
	[Contenido] [varchar](150) NOT NULL,
	[Imagen] [varchar](50) NULL,
	[Categoria] [varchar](50) NULL,
	[FechaCreacion] [date] NULL,
	[Estado] [tinyint] NULL,
 CONSTRAINT [PK_blog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
