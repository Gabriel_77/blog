﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using blog.Models;

namespace blog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {

        // GET: api/Pots
        [HttpGet]
        public IEnumerable<Models.Post> GetPosts()
        {
            using (var context=new Models.BlogDBContext())
            {
                return context.Blogs.Where(b=>b.Estado==1).ToList();
            }
        }

        // GET: api/Pots/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Post>> GetPost(int id)
        {
            using (var context = new Models.BlogDBContext())
            {
                var post = await context.Blogs.FirstOrDefaultAsync(b=>b.Id==id && b.Estado==1);
                if (post == null)
                {
                    return NotFound();
                }

                return post;
            }            
        }

        // PUT: api/Pots/5

        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchPost(int id, [FromBody] Post post)
        {
            using (var context = new Models.BlogDBContext())
            {
                post.FechaCreacion = DateTime.Today;
                post.Estado = 1;
                context.Entry(post).State = EntityState.Modified;
                try
                {
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return NoContent();
        }


        // POST: api/Pots

        [HttpPost]
        public ActionResult<Post> PostPost(Post post)
        {
            using (var context = new Models.BlogDBContext())
            {
                post.FechaCreacion = DateTime.Today;
                post.Estado = 1;
                context.Blogs.Add(post);
                context.SaveChanges();

                return CreatedAtAction("GetBlog", new { id = post.Id }, post);
            }
               
        }

        // DELETE: api/Pots/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            using (var context = new Models.BlogDBContext())
            {
                var post = await context.Blogs.FindAsync(id);
                if (post == null)
                {
                    return NotFound();
                }
                post.Estado = 0;
                //context.Blogs.Remove(blog);
                await context.SaveChangesAsync();

                return NoContent();
            }
        }

        private bool PostExists(int id)
        {
            using (var context = new Models.BlogDBContext())
            {
                return context.Blogs.Any(e => e.Id == id);
            }
        }
    }
}
