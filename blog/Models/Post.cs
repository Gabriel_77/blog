﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace blog.Models
{
    public partial class Post
    {
        public int Id { get; set; }
        [Required]
        [MinLength(2, ErrorMessage = "Debe ingresar al menos 2 caracteres")]
        public string Titulo { get; set; }
        [Required]
        [MinLength(10, ErrorMessage = "Debe ingresar no menos 10 caracteres")]
        public string Contenido { get; set; }
        public string Imagen { get; set; }
        public string Categoria { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public byte Estado { get; set; }
    }
}
